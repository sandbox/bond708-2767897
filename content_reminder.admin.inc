<?php

/*
 * Private function to get all nodes that require updates.
 */

function _get_required_nodes() {
  $orcond = db_or();
  $types = _get_reminder_types();
  $orcondswitch = false;
  foreach ($types as $type) {
    if (variable_get('content_reminder_types_' . $type->type, 0) == 1) {
      $dband = db_and();
      $duedate = time() - (variable_get('content_reminder_types_' . $type->type . '_days', 20) * 86400);
      $dband->condition('n.type', $type->type, '=');
      $dband->condition('n.status', 1, '=');
      $dband->condition('nr.timestamp', $duedate, '<');
      watchdog('content-reminder','$duedate = ' . $duedate);
      $orcond->condition($dband);
      $orcondswitch = true;
    }
  }

  if ($orcondswitch == true) {
    $query = db_select('node', 'n');
    $query->leftjoin('node_revision', 'nr', 'nr.vid = n.vid');
    $query->leftjoin('users', 'u', 'u.uid = nr.uid');
    $query->leftjoin('field_data_field_contentexpert_cr', 'ce', 'n.nid=ce.entity_id');
    $query->join('field_data_field_tax_email', 'te', 'ce.field_contentexpert_cr_tid = te.entity_id');
    $query->fields('n', array('nid', 'title', 'changed', 'type',))
            ->fields('nr', array('status'))
            ->fields('u', array('mail'))
            ->fields('ce', array('field_contentexpert_cr_tid'))
            ->fields('te', array('field_tax_email_value'));
    $query->condition($orcond)
            ->groupBy('n.nid')
            ->orderBy('n.type', 'ASC');
    $results_expert = $query->execute()->fetchAll();

    $query = db_select('node', 'n');
    $query->leftjoin('node_revision', 'nr', 'nr.vid = n.vid');
    $query->leftjoin('users', 'u', 'u.uid = nr.uid');
    $query->leftjoin('field_data_field_contenteditor_cr', 'ce', 'n.nid=ce.entity_id');
    $query->join('field_data_field_tax_email', 'te', 'ce.field_contenteditor_cr_tid = te.entity_id');
    $query->fields('n', array('nid', 'title', 'changed', 'type',))
            ->fields('nr', array('status'))
            ->fields('u', array('mail'))
            ->fields('ce', array('field_contenteditor_cr_tid'))
            ->fields('te', array('field_tax_email_value'));
    $query->condition($orcond)
            ->groupBy('n.nid')
            ->orderBy('n.type', 'ASC');
    $results_editor = $query->execute()->fetchAll();
    return $results = array_merge($results_expert, $results_editor);
  } else {   
    return NULL;
  }
}

/*
 * Private function to send an e-mail with nodes that require an update.
 *
 * @param array $nodes 
 *   List with selected nodes that need a reminder.
 * @param string $mailtype
 *   String that shows if reminder is made or an expiration.
 */
function _prepare_nodes($nodes, $mailtype) {
  $loaded_node = array();
  foreach ($nodes as $node) {
    $loaded_node[$node->nid] = node_load($node->nid);
  }
  // $emailremindernodes -> Array that will contain the node url's and
  // emailaddresses to send the reminder to if applicable.
  $emailremindernodes = array();

  // Get variabele which tells to send it to author
  $send_to_author = variable_get('content_reminder_option_author');

  foreach ($loaded_node as $node) {
    if ($send_to_author && !empty($node->uid) && $node->uid > 1) {
      // Send e-mail to the author, not the person who modified it.
      // Also the admin is excluded.
      $author = user_load($node->uid);
      $emailremindernodes = _set_emailremindernodes($node, $author->mail, $emailremindernodes);
      watchdog('content-reminder', 'author ' . $author->mail . ' is added to list');
    }
    // Load contentexpert when it is used.
    if (!empty($node->field_contentexpert_cr['und'][0])) {
      $contentexpert = taxonomy_term_load($node->field_contentexpert_cr['und'][0]['tid']);
    }
    // Load contenteditor when it is used.
    if (!empty($node->field_contenteditor_cr['und'][0])) {
      $contenteditor = taxonomy_term_load($node->field_contenteditor_cr['und'][0]['tid']);
    }
    // When expert and editor are equal, add node only once.
    if (isset($contentexpert) && isset($contenteditor) &&
            ($contentexpert->field_tax_email['und'][0]['value'] ==
            $contenteditor->field_tax_email['und'][0]['value'])) {
      $emailremindernodes = _set_emailremindernodes($node, $contentexpert->field_tax_email['und'][0]['value'], $emailremindernodes);
      watchdog('content-reminder', 'Both are added to list as ' . $contentexpert->field_tax_email['und'][0]['value']);
    } else {
      // Add expert to array when available.
      if (isset($contentexpert) && !empty($contentexpert)) {
        watchdog('content-reminder', 'contentexpert is added to list as ' . $contentexpert->field_tax_email['und'][0]['value']);
        $emailremindernodes = _set_emailremindernodes($node, $contentexpert->field_tax_email['und'][0]['value'], $emailremindernodes);
      }
      // Add editor to array when available.
      if (isset($contenteditor) && !empty($contenteditor)) {
        $emailremindernodes = _set_emailremindernodes($node, $contenteditor->field_tax_email['und'][0]['value'], $emailremindernodes);
        watchdog('content-reminder', 'contenteditor is added to list as ' . $contenteditor->field_tax_email['und'][0]['value']);
      }
    }
    // Finally add the people who subsribed to receive this mail. 
    // but shoud this be part of the content-reminder??
    if (!empty($node->field_reminder_email)) {
      foreach ($node->field_reminder_email['und'] as $mail_list) {
        foreach ($mail_list as $key => $value) {
          $emailremindernodes[$value][$node->nid]->nid = $node->nid;
          $emailremindernodes[$value][$node->nid]->title = $node->title;
          $emailremindernodes[$value][$node->nid]->changed = $node->changed;
          $emailremindernodes[$value][$node->nid]->type = $node->type;
          $emailremindernodes[$value][$node->nid]->mail = $value;
          $emailremindernodes[$value][$node->nid]->status = $node->status;
          $emailremindernodes[$value][$node->nid]->url = l($node->title, 'node/' . $node->nid, array('absolute' => TRUE));
          watchdog('content-reminder', 'emailreminder is added for ' . $value);
        }
      }
    }
  }
  _send_mails($emailremindernodes, $mailtype); // Mail the people defined in the node.
}

/*
 * Private function to get reminder node types.
 */

function _get_reminder_types() {

  $types = node_type_get_types();

  return $types;
}

/*
 * Private function to fill array.
 */

function _set_emailremindernodes($node, $value, $emailremindernodes) {
  watchdog('content-reminder','_set_emailremindernodes ' . $value . '+' . $node->title);
  $emailremindernodes[$value][$node->nid] = new stdClass();
  $emailremindernodes[$value][$node->nid]->nid = $node->nid;
  $emailremindernodes[$value][$node->nid]->title = $node->title;
  $emailremindernodes[$value][$node->nid]->changed = $node->changed;
  $emailremindernodes[$value][$node->nid]->type = $node->type;
  $emailremindernodes[$value][$node->nid]->mail = $value;
  $emailremindernodes[$value][$node->nid]->status = $node->status;
  $emailremindernodes[$value][$node->nid]->url = l($node->title, 'node/' . $node->nid, array('absolute' => TRUE));
  return $emailremindernodes;
}

/*
 * Private function to get expired nodes and update the status to 0 if needed.
 */

function _get_old_nodes() {

  $orcond = db_or();
  $types = _get_reminder_types();
  $orcondswitch = false;

  foreach ($types as $type) {
    if (variable_get('content_reminder_types_' . $type->type . '_depub_box', 0) == 1) {
      $dband = db_and();
      $duedate = time() - (variable_get('content_reminder_types_' . $type->type . '_depub', 20) * 86400);
      $dband->condition('n.type', $type->type, '=');
      $dband->condition('n.status', 1, '=');
      $dband->condition('nr.timestamp', $duedate, '<');
      $orcond->condition($dband);
      $orcondswitch = true;
    }
  }

  if ($orcondswitch == true) {
    $query = db_select('node', 'n');
    $query->leftjoin('node_revision', 'nr', 'nr.vid = n.vid');
    $query->leftjoin('users', 'u', 'u.uid = nr.uid');
    $query->fields('n', array('nid', 'title', 'changed', 'type'))
            ->fields('nr', array('status'))
            ->fields('u', array('mail'));
    $query->condition($orcond)
            ->groupBy('n.nid')
            ->orderBy('n.type', 'ASC');
    $mailnodes = $query->execute()->fetchAll();

    $workbench_exists = module_exists('workbench_moderation');
    if ($workbench_exists) {
      $workbench_to_state = workbench_moderation_state_none();
    }
    foreach ($types as $type) {
      if (variable_get('content_reminder_types_' . $type->type . '_depub_box', 0) == 1) {
        $query = db_select('node_revision', 'nr');
        $query->leftjoin('node', 'n', 'n.vid = nr.vid');
        $query->fields('n', array('nid'));
        $query->condition($orcond);
        $results = $query->execute()->fetchAll();
        foreach ($results as $result) {
          foreach ($mailnodes as &$mailnode) {
            if ($result->nid == $mailnode->nid) {
              $mailnode->status = 0;
            }
          }
          $node = node_load($result->nid);
          // Unpublish the current live revision.
          // @see workbench_moderation_node_history_view()
          // @see workbench_moderation_node_unpublish_form()
          if ($workbench_exists && !empty($node->workbench_moderation['published']) && $node->vid == $node->workbench_moderation['published']->vid) {
            // Remove the moderation record's "published" flag.
            $query = db_update('workbench_moderation_node_history')
                    ->condition('hid', $node->workbench_moderation['published']->hid)
                    ->fields(array('published' => 0))
                    ->execute();

            // Moderate the revision.
            workbench_moderation_moderate($node, $workbench_to_state);

            // Make sure the 'current' revision is the 'live' revision -- ie, the revision
            // in {node}.
            $live_revision = workbench_moderation_node_current_load($node);
            $live_revision->status = 0;
            $live_revision->revision = 0;
            $live_revision->workbench_moderation['updating_live_revision'] = TRUE;
            node_save($live_revision);
          } else {
            $node->status = 0;
            node_save($node);
          }
          $a[$result->nid]->status = 0;
        }
      }
    }
    return $mailnodes;
  } else {
    return NULL;
  }
}

/*
 * Private function to send e-mails
 */

function _send_mails($mailnodes, $mailtype) {
  watchdog('content-reminder','system is trying to send mail');
  foreach ($mailnodes as $mail => $nodes) {
    $build = array(
        '#theme' => 'content_reminder',
        '#nodes' => $nodes,
        '#mailtype' => $mailtype,
        '#content_reminder_text' => variable_get('content_reminder_' . $mailtype . '_text', 'Inhoud die verlopen is of binnenkort verloopt.'),
    );

    if ($mailtype == 'expire') {
      $subject = variable_get('content_reminder_expire_subject', '@count item(s) zijn verlopen en offline geplaatst');
    } else {
      $subject = variable_get('content_reminder_reminder_subject', 'Je moet @count item(s) bekijken');
    }
    $subject = str_replace('@count', count($nodes), $subject);

    $mail_token = microtime();
    $from = variable_get('site_mail');
    $message = array(
        'id' => 'custom' . '_' . $mail_token,
        'to' => $mail,
        'subject' => $subject,
        'body' => drupal_render($build),
        'headers' => array(
            'From' => $from,
            'Sender' => $from,
            'Return-Path' => $from,
            'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
        ),
    );
    $system = drupal_mail_system('custom', $mail_token);
    $system->mail($message);
  }
}
